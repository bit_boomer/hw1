# Лабораторная работа 1  

docs - документация на проект.  
src - исходники.  
fakelog - скрипт для генерации данных.  
guide.docx - руководство для сборки и использования.  
pom.xml - конфигурационный файл для сборки.  
Report.zip - отчет о лабораторной работе.  

# Требования  
1. RPM-based дистрибутив Linux с ядром версии не ниже 3.10 с доступом в интернет.  
2. JDK 1.8  
3. Hadoop 2.10.0  
4. Пакет snappy-1.1.0  
5. maven  
Опционально для генератора данных:  
6. perl5  
7. cpan  
8. Библиотека Data::Faker из CPAN  
# Сборка  
1. Выкачиваем директорию src и файл pom.xml  
2. В директории сборки запускаем   
mvn install  
для тестирования и сборки. Итоговый JAR будет расположен в директории target.  

# Запуск  
1. Необходимо запустить Namenode из директории где установлен hadoop.        
   ```console
    foo@bar:/opt/hadoop-2.10.0#sbin/start-dfs.sh
    ```   
2. Необходимо загрузить в целевую директорию hdfs входные данные.  
   ```console
    foo@bar:/opt/hadoop-2.10.0$hadoop dfs -put <локальный путь> <путь в DFS>
    ```  
3. Для запуска непосредственно приложения выполнить команду:     
    ```console
    foo@bar:~/HW1-Haloop$ jar ./target/HW1-Haloop-1.0-SNAPSHOT-jar-with-dependencies.jar <входная директория в hdfs> <выходная директория в hdfs>  
    ```  

# Генератор данных   
1. Выкачать директорию fakelog.  
2. Установить cpan:  
    ```console
    foo@bar:~#yum install cpan
    ```   
3. Установить Data::Faker:  
    ```console
    foo@bar:~#cpan install Data::Faker
    ```  
4. Находясь в директории fakelog запустить генератор командой:  
    ```console
    foo@bar:~/fakelog$ perl ./fakelog.pl <количество строк> <процент неверных строк> <путь выходного файла>  
    ```  
 
 
## Текущий статус
|Требование лабораторной  |Статус   |
|---|---|
|For input information from a)-2 count how many users of IE, Mozilla or other were detected   |  + |
|SequenceFile with Snappy encoding (Read content of compressed file from console using command line)   | +  |
|Combiner is used   | + |
|Репорт | +  |
  
|Требование репорта  |Статус   |
|---|---|
|1.	IDE agnostic build: Maven, Ant, Gradle, sbt, etc (10 points)| + |
|2.	Unit tests are provided (20 points) | + |
|3.	Code is well-documented (10 points) | + |
|4.	Script for input file generation or extraction to HDFS must be provided (10 points) | + |
|5.	Working MapReduce application that corresponds business logic, input/output format and additional Requirements that has been started on Hadoop cluster (not singleton mode) (30 points) | + |
